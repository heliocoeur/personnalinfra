import math

def Syracuse(u):
    if u%2==0:
        z=u//2
    else:
        z=3*u+1
    return z

def ListeSyracuse(u):
    L=[u]
    z=u
    while u!=1:
        u=Syracuse(u)
        L.append(u)
        # print(L)
    return L

def n(u):
    T=[u]
    T.append(u)
    while u > 0:
        ListeSyracuse(u)
        u=u+1
        print('good for u', u)
    return u


# print(n(7))
print(ListeSyracuse(7))
