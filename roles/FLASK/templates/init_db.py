import sqlite3
import pandas as pd

connection = sqlite3.connect('database.db')
c = connection.cursor()

# load the data into a Pandas DataFrame
gunshots = pd.read_csv('gunshots.csv')
# write the data to a sqlite table
gunshots.to_sql('gunshots', connection, if_exists='append', index = False)

# with open('schema.sql') as f:
#     connection.executescript(f.read())
#
#
# connection.commit()
# connection.close()
