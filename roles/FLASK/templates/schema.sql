DROP TABLE IF EXISTS posts;

CREATE TABLE gunshots (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    incidentID TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    incidentDate TEXT NOT NULL,
    state TEXT NOT NULL,
    city  TEXT,
    Address TEXT,
    Killed TEXT,
    Injured TEXT,
    Operation TEXT,
);