create database frederick;
create user frederick with encrypted password 'Ragnarok/09';
grant all privileges on DATABASE frederick to frederick;

create database lamaprod;
create user lamaprod with encrypted password 'Ragnarok/09';
grant all privileges on DATABASE lamaprod to lamaprod;