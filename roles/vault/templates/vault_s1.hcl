ui = true

listener "tcp" {
  address          = "0.0.0.0:8200"
  tls_disable      = "true"
}

storage "consul" {
  address = "{{ VM_IP }}:8500"
  path    = "vault/"
}

