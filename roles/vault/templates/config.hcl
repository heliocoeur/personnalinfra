listener "tcp" {
  address          = "0.0.0.0:8200"
  cluster_address  = "{{ VM_IP }}:8201"
  tls_disable      = "true"
}

storage "consul" {
  address = "{{ VM_IP }}:8500"
  path    = "vault/"
}
